# A docker file for gitlab CI-ing this project
FROM blang/latex:ctanbasic

# This is adapted from blang/latex
# FROM ubuntu:xenial
# ENV DEBIAN_FRONTEND noninteractive

# RUN apt-get update -q \
#     && apt-get install -qy \
#     build-essential \
#     wget \
#     zip \
#     libfontconfig1 \
#     && rm -rf /var/lib/apt/lists/*

# # Install TexLive with scheme-basic
# RUN wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz; \
# 	mkdir /install-tl-unx; \
# 	tar -xvf install-tl-unx.tar.gz -C /install-tl-unx --strip-components=1; \
#     echo "selected_scheme scheme-basic" >> /install-tl-unx/texlive.profile; \
# 	/install-tl-unx/install-tl -profile /install-tl-unx/texlive.profile; \
#     rm -r /install-tl-unx; \
# 	rm install-tl-unx.tar.gz
	
# ENV PATH="/usr/local/texlive/2018/bin/x86_64-linux:${PATH}"

# ENV HOME /data
# WORKDIR /data

# # Install latex packages
# RUN tlmgr install latexmk

# Install my GURPS LaTeX Package
# TODO make this a tagged release
# RUN apt-get update -q \
#     && apt-get install -qy \
#     build-essential \
#     wget \
#     zip \
#     libfontconfig1 \
#     && rm -rf /var/lib/apt/lists/*

# For some crazy reason, it own't make the first time, but it will the second.
# TODO figure out why
RUN apt-get update -q \
     && apt-get install -qy \
     build-essential \
     git \
#     wget \
     zip \
    && wget http://mirror.ctan.org/systems/texlive/tlnet/update-tlmgr-latest.sh \
    && chmod +x update-tlmgr-latest.sh \
    && ./update-tlmgr-latest.sh \
    && tlmgr update --self \
    && tlmgr install \
      luatex \
      etoolbox \
      l3packages \
      tools \
      luacode \
      pgf \
      enumitem \
      hyperref \
      xstring \
      xkeyval \
      l3kernel \
      luatexbase \
      ctablestack \
      xcolor \
      blindtext \
      thumbpdf \
    && git -C /opt clone https://github.com/nasfarley88/gurps-latex-package \
    && make -C /opt/gurps-latex-package/source inst \
    ; make -C /opt/gurps-latex-package/source inst \
    && rm -rf /var/lib/apt/lists/*

VOLUME ["/data"]

# Install the latest GCS (GURPS Character Sheet)
RUN mkdir -p /opt/gcs \
    && cd /opt/gcs \
    && wget -SL https://github.com/richardwilkes/gcs/releases/download/gcs-4.11.1/gcs-4.11.1.tgz --output-document=gcs.tgz \
    && tar -xvzf gcs.tgz


ENV PATH /opt/gcs/gcs-4.11.1:$PATH
